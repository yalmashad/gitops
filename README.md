```sh
# install ArgoCD in k8s
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

# access ArgoCD UI at https://argocd.f5demo.io
kubectl apply -f transportserver.yaml 

# login with admin user and below token:
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode && echo

# Optionally Login Using Argo CD CLI
brew install argocd
argocd admin initial-password -n argocd
argocd login argocd.f5demo.io

# Change the password
argocd account update-password

# Add argocd application config
kubectl apply -f example-application.yaml
```
