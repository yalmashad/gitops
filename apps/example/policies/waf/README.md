For VirtualServer, you can apply a policy:
* to all routes (spec policies)
* to a specific route (route policies)

Route policies of the *same type* override spec policies.
The overriding is enforced by NGINX: the spec policies are implemented in the `server` context of the config, and the route policies are implemented in the `location` context. As a result, the route policies of the same type win.