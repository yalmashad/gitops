Add the following to the ConfigMap object of NGINX Ingress Controller (Helm auto-generate one by default)

```
data:
  resolver-addresses: <kubeDNS IP>
  resolver-valid: 5s
```
Example:

```
$ kubectl -n kube-system get svc kube-dns
NAME       TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)         AGE
kube-dns   ClusterIP   10.4.0.10    <none>        53/UDP,53/TCP   9d
```
```
kubectl edit configmaps my-ingress-nginx-ingress
```
```
kind: ConfigMap
apiVersion: v1
metadata:
  name: my-ingress-nginx-ingress
  namespace: nginx-ingress
data:
  resolver-addresses: 10.4.0.10
  resolver-valid: 5s
  ```
Okta user: user@f5demo.io
